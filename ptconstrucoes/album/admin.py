from django.contrib import admin
from .models import Album, Foto


class AlbumAdmin(admin.ModelAdmin):
    list_display = ('id', 'titulo', 'data_criacao')
    list_filter = ('tipo_album',)
    search_fields = ('titulo',)


class FotoAdmin(admin.ModelAdmin):
    list_display = ('id', 'id_album', 'descricao', 'data_criacao')
    list_filter = ('id_album',)
    search_fields = ('descricao',)


admin.site.register(Album, AlbumAdmin)
admin.site.register(Foto, FotoAdmin)

