from django.db import models
from cloudinary.models import CloudinaryField

TIPO_CHOICES = (
    ('Construcao', 'Construcao'),
    ('Reforma', 'Reforam'),
    ('Projeto', 'Projeto'),
)


class Album(models.Model):
    titulo = models.CharField(max_length=80)
    tipo_album = models.CharField(max_length=50, choices= TIPO_CHOICES)
    descricao = models.TextField(max_length=500)
    imagem = CloudinaryField('capa', null=True, blank=True)
    data_criacao = models.DateTimeField(auto_created=True)
    ativo = models.BooleanField(default=True)

    class Meta:
        ordering = ('-data_criacao',)

    def __str__(self):
        return self.titulo

class Foto(models.Model):
    id_album = models.ForeignKey(Album, on_delete=models.PROTECT)
    descricao = models.CharField(max_length=50)
    foto = CloudinaryField('Foto', null=True, blank=True)
    data_criacao = models.DateTimeField(auto_created=True)
    ativo = models.BooleanField(default=True)

    class Meta:
        ordering = ('-data_criacao',)

    def __str__(self):
        return self.descricao




